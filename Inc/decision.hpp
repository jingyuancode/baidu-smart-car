#ifndef __DECISION_HPP__
#define __DECISION_HPP__

extern int decision_state;
extern bool decision_slope_flag;

void decision();
void decision_slope();

#endif
