#ifndef __INTERFACE_HPP__
#define __INTERFACE_HPP__

extern float interface_angle_rate;
extern float interface_speed;

void interface_init();
void interface_in();
void interface_out(unsigned char* data);

#endif