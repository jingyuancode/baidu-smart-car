#ifndef __PLANNING_HPP__
#define __PLANNING_HPP__

#include "main.hpp"
#include "opencv2/opencv.hpp"
#include "map.hpp"

class point
{
private:
public:
    int x;
    int y;
    point();
    point(int x, int y);
};

extern float planning_angle_current;
extern float planning_distance_error;
extern bool planning_zebra_flag;

void planning();
cv::Mat planning_img_preprocess(cv::Mat img);
void planning_point_generate(map plan_map, point* points);
void planning_trace_generate(cv::Mat img, point points[]);
void planning_white_count(cv::Mat runable, int* white_count_array);
int planning_white_max(int* white_count_array);
bool planning_zebra_check(cv::Mat img);
int planning_circle_check(cv::Mat img);

#endif