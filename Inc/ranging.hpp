#ifndef __RANGING_HPP__
#define __RANGING_HPP__

#include "opencv2/opencv.hpp"

float ranging(cv::Mat frame, cv::Mat cameraMatrix, float height);

#endif