#ifndef __MAP_HPP__
#define __MAP_HPP__
#define MAP_WIDTH 201
#define MAP_LENGTH 301

#include "opencv2/opencv.hpp"

class map
{
private:
    float hor_angle;
    float height;
    cv::Mat map_mat = cv::Mat(MAP_LENGTH, MAP_WIDTH, CV_8UC1, cv::Scalar(0));
    cv::Mat map_mat_no_erode = cv::Mat(MAP_LENGTH, MAP_WIDTH, CV_8UC1, cv::Scalar(0));
public:
    int start_y;
    map();
    map(float height);
    ~map();
    void write(int x, int y, int value);
    char read(int x, int y);
    void generate(cv::Mat img, cv::Mat camera_matrix);
    void map_erode(int k);
    cv::Mat get_map();
    void set_map(cv::Mat map);
};

#endif