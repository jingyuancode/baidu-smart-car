#ifndef __CONTROLLER_H__
#define __CONTROLLER_H__

#include "usb_can.hpp"
void controller();
void controller_input(float angle, float speed);
void controller_follow();
void controller_left_part();
void controller_right_part();
void controller_stop();

#endif