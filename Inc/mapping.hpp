#ifndef __MAPPING_HPP__
#define __MAPPING_HPP__

#include "opencv2/opencv.hpp"
#include "map.hpp"

extern map mapping_local;

void mapping();
cv::Mat mapping_img_preprocess(cv::Mat img);

#endif