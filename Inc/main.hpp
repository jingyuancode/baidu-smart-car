#ifndef __MAIN_HPP__
#define __MAIN_HPP__

#include <iostream>
#include <thread>
#include "opencv2/opencv.hpp"

#define CAMERA_HEIGHT 18

extern cv::Mat frame;
extern bool ready;
extern unsigned char frame_cnt;
extern cv::Mat cameraMatrix;

#endif