#ifndef __DETECTION_HPP__
#define __DETECTION_HPP__

#include "main.hpp"
#include "opencv2/opencv.hpp"

extern unsigned char detection_flag;
extern int detection_index;
extern cv::Rect detection_box;

void detection();

#endif