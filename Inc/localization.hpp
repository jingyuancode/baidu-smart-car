#ifndef __LOCALIZATION_HPP__
#define __LOCALIZATION_HPP__

extern float localization_odometer;
extern float localization_yaw_angle;

void localization();

#endif