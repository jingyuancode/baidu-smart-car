#include "main.hpp"
#include "decision.hpp"
#include "detection.hpp"
#include "interface.hpp"
#include "planning.hpp"
#include <iostream>
#include "unistd.h"
int decision_state = 0;//state flag for the whole system.
bool decision_slope_flag = 0;
//0: start
//1: follow
//2: left part
//3: right part
//4: circle
//5: stop

void decision()
{
    while (!ready);
    decision_state = 1;
    while (1)
    {
        if(planning_zebra_flag)
        {
            decision_state = 5;
        }
        else if(detection_flag && decision_state == 1)
        {
            if(detection_index < 2)
            {
                decision_state = 2;
            }
            else
            {
                decision_state = 3;
            }
            while (detection_flag);
        }
        else if(decision_state != 5)
        {
            decision_state = 1;
        }
       // std::cout << "decision_state:" << decision_state << std::endl;
    }
    
}

void decision_slope()
{
    int cnt = 0;
    while(1)
    {
	//std::cout << "speed:" << interface_speed << "c:" << cnt <<  std::endl;
        if(decision_state == 1 && interface_speed < 0.00001)
	{
	    cnt++;
	    sleep(1);
	}
	else
	{
	    cnt = 0;
	}
	if(cnt > 5)
	{
	    decision_slope_flag = true;
	    sleep(1);
	}
	else
	{
	    decision_slope_flag = false;
	}
    }
}
