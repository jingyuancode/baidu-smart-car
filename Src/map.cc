#include "map.hpp"
#include <iostream>
//using namespace std;
using namespace cv;

map::map()
{
    height = 0;
    start_y = 0;
}

map::map(float height)
{
    this->height = height;
    start_y = 0;
}

map::~map()
{
}

void map::generate(Mat img, Mat camera_matrix)
{
    Mat coordinate = (Mat_<double>(3,1) << 0, 0, 1);
    float x = 0, y = 0, z = 0;
    y = height;
    for(z = MAP_LENGTH - 1; z > 0; z-=1)
    {
        for(x = -(MAP_WIDTH - 1) / 2; x <= ((MAP_WIDTH - 1) / 2); x+=1)
        {
            coordinate.at<double>(0, 0) = x / z;
            coordinate.at<double>(1, 0) = y / z;
            Mat img_coordinate = camera_matrix * coordinate;
            int img_x = img_coordinate.at<double>(0, 0);
            int img_y = img_coordinate.at<double>(1, 0);
            if(img_x < 0)continue;
            if(img_x > img.cols)break;
            if(img_y < 0)continue;//useless
            if(img_y > img.rows)break;
            this->write(x, z, img.at<unsigned char>(img_y, img_x));
            if(img.at<unsigned char>(img_y, img_x))
            {
                start_y = z;
            }
        }
    }
    for(int i = 0; i < map_mat.cols; i++)
    {
        if(map_mat.at<unsigned char>(start_y, i))
        {
            //line(map_mat, Point(i, 0), Point(i, start_y), Scalar(255));
        }
    }
}

void map::write(int x, int y, int value)
{
    if(x >= - (MAP_WIDTH - 1) / 2 && x <= ((MAP_WIDTH - 1) / 2))
    {
        int array_x = x + (MAP_WIDTH - 1) / 2;
        int array_y = y;
        map_mat.at<unsigned char>(array_y, array_x) = value;
    }
}

char map::read(int x, int y)
{
    if(x >= - (MAP_WIDTH - 1) / 2 && x <= ((MAP_WIDTH - 1) / 2))
    {
        int array_x = x + MAP_WIDTH / 2;
        int array_y = y;
        return map_mat.at<unsigned char>(array_y, array_x);
    }
    else
    {
        return 0;
    }
}

void map::map_erode(int k)
{
    Mat kernal = getStructuringElement(MORPH_ELLIPSE, Size(k, k));
    erode(map_mat, map_mat, kernal);
}

Mat map::get_map()
{
    return map_mat.clone();
}

void map::set_map(Mat map)
{
    this->map_mat = map.clone();
}