//Ota test
#include "main.hpp"
#include "planning.hpp"
#include "controller.hpp"
#include "detection.hpp"
#include "interface.hpp"
#include "decision.hpp"
#include "localization.hpp"
#include "mapping.hpp"

using namespace std;
using namespace cv;

Mat frame;
bool ready = 0;
unsigned char frame_cnt = 0;

Mat cameraMatrix = (Mat_<double>(3,3) << 
    1149.51797346866,	0,	643.259600655449,
    0,	1149.33614190820,	385.700384661007,
    0,	0,	1);
//Mat cameraMatrix = (Mat_<double>(3,3) << 
//    503.201841720023,   0,                  317.956060314804,
//    0,                  502.461030091851,   231.433435589884,
//    0,                  0,                  1);

void onMouse(int event, int cols, int rows, int flags, void* param)
{
	//Mat* im = reinterpret_cast<Mat*>(param);
	Mat* im = (Mat*)param;
	switch (event)
	{
	case EVENT_LBUTTONDOWN://左键按下显示像素值
		string point = "<" + to_string(cols / 2) + " " + to_string(rows / 2) + ">";
		cv::putText(*im, point, cv::Point(cols, rows), 1, 1, cv::Scalar(0, 0, 255), 1);
        cout << point << endl;
	}
    //cout << "down" << endl;
}

int main()
{
    cout << "Hello World" << endl;
    thread planner(planning);
    thread control(controller);
    //thread detector(detection);
    thread interface(interface_in);
    thread decider(decision);
    thread slope_observer(decision_slope);
    thread locator(localization);
    //thread mapper(mapping);

    VideoCapture cap("/dev/video0");
    cap.set(CAP_PROP_FOURCC, VideoWriter::fourcc('M', 'J', 'P', 'G'));
    cap.set(CAP_PROP_FPS, 90);
    cap.set(CAP_PROP_FRAME_HEIGHT, 720);
    cap.set(CAP_PROP_FRAME_WIDTH, 1280);
    cout << cap.get(CAP_PROP_FRAME_HEIGHT) << endl;
    cout << cap.get(CAP_PROP_FRAME_WIDTH) << endl;
    
    //frame = imread("../Test/cross.jpg");//for test
    //frame = imread("../Resource/plate.png");//for test
    //namedWindow("frame");
    //setMouseCallback("frame", onMouse, (void*)&frame);

    interface_init();

    planner.detach();
    control.detach();
    //detector.detach();
    interface.detach();
    decider.detach();
    slope_observer.detach();
    locator.detach();
    //mapper.detach();

    cap >> frame;
    ready = true;
    while (waitKey(1) != 'q')
    {
        cap >> frame;
        frame_cnt++;
        imshow("frame", frame);
    }
    

    return 0;
}
