#include "controller.hpp"
#include "planning.hpp"
#include <cstring>
#include "opencv2/opencv.hpp"
#include "unistd.h"
#include <iostream>
#include "interface.hpp"
#include "decision.hpp"
#include "localization.hpp"
#include "ranging.hpp"

//controller is a state machine for decision

void controller()
{
    while (1)
    {
        switch (decision_state)
        {
        case 0:
            controller_input(0, 0);
            break;

        case 1:
            controller_follow();
            //std::cout << "follow" << std::endl;
            break;

        case 2:
            controller_left_part();
            break;

        case 3:
            controller_right_part();
            break;

        case 5:
            controller_stop();
            break;

        default:
            break;
        }
    }
}

void controller_input(float angle, float speed)
{
    unsigned char data[8] = {};
    memcpy(data, &angle, 4);
    memcpy(data + 4, &speed, 4);
    //std::cout << "angle:" << angle << std::endl;
    //std::cout << "speed:" << speed << std::endl;
    interface_out(data);
}

void controller_slope()
{
    float angle_target = 0;
    float angle_error = 0;
    float kp = 0.005;
    angle_target = planning_distance_error * kp;
    angle_error = angle_target - planning_angle_current;
    if(angle_error < - 1) angle_error = - 1;
    else if(angle_error > 1) angle_error = 1;
    for(int  i = 0; i < 150; i++)
    {
	controller_input(0, -1);
	usleep(10000);
    }
    for(int i = 0; i < 150; i++)
    {
        controller_input(0, 20);
	usleep(10000);
    }	  
}

void controller_follow()
{
    float angle_target = 0;
    float angle_error = 0;
    float kp = 0.005;
    angle_target = planning_distance_error * kp;
    angle_error = angle_target - planning_angle_current;
    if(angle_error < - 1) angle_error = - 1;
    else if(angle_error > 1) angle_error = 1;
    if(decision_slope_flag)
    {
	controller_slope();
    }
    else
    {
    	controller_input(angle_error, 3.3);//TODO
    }
    usleep(10000);
    //std::cout << "follow" << std::endl;
}

void controller_left_part()
{
    std::cout << "left_part" << std::endl;
    float start_odometer = localization_odometer;
    float distance = ranging(frame, cameraMatrix, CAMERA_HEIGHT);
    while (localization_odometer < start_odometer + distance + 0)//const for delay
    {
        controller_follow();
    }
    
    float start_yaw_angle = localization_yaw_angle;
    while (localization_yaw_angle < localization_yaw_angle + 0.3)//TODO const for delay
    {
        controller_input(-1, 0.02);
        usleep(10000);
    }

    while (localization_yaw_angle >= start_yaw_angle)
    {
        controller_input(-1, -0.02);
        usleep(10000);
    }
    
}

void controller_right_part()
{
    for (int i = 0; i < 300; i++)
    {
        controller_follow();
        usleep(10000);
    }

    for (int i = 0; i < 300; i++)
    {
        controller_input(0, 0);
        usleep(10000);
    }
}

void controller_stop()
{
    std::cout << "stop" << std::endl;
    float start_odometer = localization_odometer;
    cv::imshow("stop", frame);
    for(int i = 0; i < 100; i++)
    {
	controller_follow();
    }
    //controller_follow();
    while (1)
    {
        controller_input(0, 0);
        usleep(10000);
    }
    
}
