#include "detection.hpp"
#include <iostream>
#include <ctime>

using namespace cv;
using namespace std;

unsigned char detection_flag = 0;
int detection_index = 0;

Rect detection_box;

void detection()
{
    char frame_cnt_last = 0;
    Mat target_mask[4];
    target_mask[0] = imread("../Resource/0.png", IMREAD_GRAYSCALE);
    target_mask[1] = imread("../Resource/1.png", IMREAD_GRAYSCALE);
    target_mask[2] = imread("../Resource/2.png", IMREAD_GRAYSCALE);
    target_mask[3] = imread("../Resource/3.png", IMREAD_GRAYSCALE);
    Mat img;
    Mat img_hsv;
    Mat img_hsv_list[3];
    Mat img_h_mask_high, img_h_mask_low, img_h_mask, img_s_mask, img_v_mask;
    Mat img_hs_mask;
    Mat img_red;
    Mat mask_xor;
    Mat target;

    float index_threshold[4] = {0.32, 0.32, 0.32, 0.32};
    
    while (!ready);


    while (1)
    {
        while(frame_cnt_last == frame_cnt);
        frame_cnt_last = frame_cnt;
        img = frame.clone();
        

        clock_t begin;
        clock_t end;
        int dur = 0;
        
        GaussianBlur(img, img, Size(3, 3), 15);
        cvtColor(img, img_hsv, COLOR_BGR2HSV);
        split(img_hsv, img_hsv_list);
        threshold(img_hsv_list[0], img_h_mask_low, 1, 255, THRESH_BINARY_INV);
        threshold(img_hsv_list[0], img_h_mask_high, 140, 255, THRESH_BINARY);
        threshold(img_hsv_list[1], img_s_mask, 70, 255, THRESH_BINARY);
        threshold(img_hsv_list[2], img_v_mask, 70, 255, THRESH_BINARY);

        bitwise_or(img_h_mask_high, img_h_mask_low, img_h_mask);
        bitwise_and(img_h_mask, img_s_mask, img_hs_mask);
        bitwise_and(img_hs_mask, img_v_mask, img_red);

        Mat kernal = getStructuringElement(MORPH_ELLIPSE, Size(9, 9));
        dilate(img_red, img_red, kernal);

        vector<vector<Point> > contours_points;
        findContours(img_red, contours_points, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);
        for (int i = 0; i < contours_points.size(); i++)
        {
            Rect box = boundingRect(contours_points[i]);
            if(box.y < 0/*TODO*/ || box.height < 70 || box.height / box.width > 1.7|| box.width / box.height > 1.5) continue;
            target = img_red(box);
            resize(target, target, Size(50, 50), INTER_NEAREST);
            int white_count = countNonZero(target);
            if(white_count < 50 * 50 / 2 || white_count > 50 * 50 * 0.9) continue;
            for(int j = 0; j < 4; j++)
            {
                bitwise_xor(target_mask[j], target, mask_xor);
                white_count = countNonZero(mask_xor);
                if(white_count < 50 * 50 * index_threshold[j])
                {
                    detection_index = j;
                    detection_flag = 150;
                    //cout << "white_count" << white_count << endl;
                    //cout << "detection_index:" << detection_index << endl;
                    detection_box = box;
                    rectangle(img, Point(box.x, box.y), Point(box.x+box.width, box.y + box.height), Scalar(0, 255, 0));
                    //waitKey(1000);
                }
                //waitKey(1000);
                //break;
            }
        }
        if(detection_flag)detection_flag --;
        //imshow("target_mask", target_mask[0]); 
        //imshow("img_h_mask", img_h_mask); 
        //imshow("img_s_mask", img_s_mask); 
        //imshow("img_v_mask", img_v_mask);
        //imshow("img_red", img_red);
        //imshow("img", img);
    }
    
}
