#include "planning.hpp"
#include "opencv2/opencv.hpp"
#include "mapping.hpp"

using namespace cv;

float planning_angle_current = 0;
float planning_distance_error = 0;

bool planning_zebra_flag = 0;
int planning_circle_flag = 0;

point::point()
{
    this->x = 0;
    this->y = 0;
}
point::point(int x, int y)
{
    this->x = x;
    this->y = y;
}

void planning()
{
    char frame_cnt_last = 0;
    while (!ready);
    frame_cnt_last = frame_cnt;
    //point plan_points[MAP_LENGTH / 10] = {};
    point plan_points[500] = {};
    //plan_pointTypedef* = plan_points;

    map plan_map(CAMERA_HEIGHT);//height of camera
    Mat runable;
    int white_count_array[640] = {};
    int white_max_index = -1;

    point left_circle_check[10] = {point(360, 360), point(360, 360)};

    while (1)
    {
        while(frame_cnt_last == frame_cnt);
        frame_cnt_last = frame_cnt;
        
        runable = planning_img_preprocess(frame);//runable is 640*360

        planning_zebra_flag = planning_zebra_check(runable);
        planning_circle_flag = planning_circle_check(runable);

        //planning_white_count(runable, white_count_array);
        //white_max_index = planning_white_max(white_count_array);
        //if(white_max_index == -1);//TODO
        //planning_distance_error = white_max_index - 320;
        //std::cout << "planning_zebra_flag" << planning_zebra_flag << std::endl;

        planning_trace_generate(runable, plan_points);
        planning_distance_error = plan_points[50].x * 0.4 + plan_points[100].x * 0.2 + plan_points[150].x * 0.2 + plan_points[200].x * 0.2;
        //planning_angle_current = - (plan_points[100].x - plan_points[0].x) * 0.01;

        //plan_map.set_map(mapping_local.get_map());
        //Mat map_mat = plan_map.get_map();
        //plan_map.map_erode(30);
        //Mat map_mat = plan_map.get_map();
        //planning_point_generate(plan_map, plan_points);
        //flip(map_mat, map_mat, 0);
        //planning_distance_error = plan_points[plan_map.start_y / 10].x;
        //planning_angle_current = - atan2((plan_points[plan_map.start_y / 10 + 1].x - plan_points[plan_map.start_y / 10 + 1].x), 10);
        
        //std::cout << "planning_distance_error" << planning_distance_error << std::endl;
        //std::cout << "planning_angle_current" << planning_angle_current << std::endl;
        //imshow("runable", runable);
    }
    
}

Mat planning_img_preprocess(Mat img)
{
    static Mat img_hsv;
    static Mat img_hsv_list[3];
    static Mat img_h_mask, img_s_mask, img_v_mask;
    static Mat img_h_mask_high, img_h_mask_low;
    static Mat img_sv_mask;
    static Mat img_white_mask, img_s_white_mask, img_v_white_mask;
    static Mat img_runable;

    resize(img, img, Size(0, 0), 0.5, 0.5);

    cvtColor(img, img_hsv, COLOR_BGR2HSV);
    split(img_hsv, img_hsv_list);
    threshold(img_hsv_list[2], img_v_mask, 100, 255, THRESH_BINARY);
    threshold(img_hsv_list[1], img_s_mask,60, 255, THRESH_BINARY_INV);
    threshold(img_hsv_list[0], img_h_mask_high, 150, 255, THRESH_BINARY);
    threshold(img_hsv_list[0], img_h_mask_low, 85, 255, THRESH_BINARY_INV);
    threshold(img_hsv_list[1], img_s_white_mask, 30, 255, THRESH_BINARY_INV);
    threshold(img_hsv_list[2], img_v_white_mask, 190, 255, THRESH_BINARY);
    bitwise_and(img_s_white_mask, img_v_white_mask, img_white_mask);
    bitwise_or(img_h_mask_low, img_h_mask_high, img_h_mask);
    bitwise_and(img_s_mask, img_v_mask, img_sv_mask);
    bitwise_and(img_h_mask, img_sv_mask, img_runable);
    bitwise_or(img_white_mask, img_runable, img_runable);
    medianBlur(img_runable, img_runable, 3);
    //imshow("img_h_mask", img_h_mask); 
    //imshow("img_s_mask", img_s_mask); 
    //imshow("img_v_mask", img_v_mask);
    imshow("img_run", img_runable);
    
    return img_runable.clone();
}

void planning_white_count(Mat img, int* white_count_array)
{
    Mat runable = img.clone();
    Mat kernal = getStructuringElement(MORPH_ELLIPSE, Size(100, 100));
    erode(runable, runable, kernal);
    // imshow("plan_run", runable);
    for(int i = 0; i < 640; i++)
    {
        int white_count = 0;
        for(int j = 350 - 1; j >= 0; j--)//y
        {
            if(runable.at<unsigned char>(j, i))
            {
                white_count ++;
            }
            else
            {
                white_count_array[i] = white_count;
                break;
            }
        }
    }
}

int planning_white_max(int* white_count_array)
{
    int max_index = -1;
    int max_value = 0;
    for (int i = 0; i < 640; i++)
    {
        if(white_count_array[i] > max_value)
        {
            max_value = white_count_array[i];
            max_index = i;
        }
    }
    return max_index;
}

void planning_trace_generate(Mat img, point points[])
{
    float k = -0.55;
    int range = 200;
    int point_index = -1;
    Mat trace = frame.clone();
    for (int i = img.rows - 1; i > 100; i--)
    {
        point_index ++;
        int left_cnt = 0, right_cnt = 0;
        int x = 0;
        int range_rows = range + k * (img.rows - i);
        for (int j = 0; j < range_rows; j++)
        {
            //left_check
            x = img.cols / 2 - j;
            if(img.at<unsigned char>(i, x))
            {
                left_cnt ++;
            }
            x = img.cols / 2 + j;
            if(img.at<unsigned char>(i, x))
            {
                right_cnt ++;
            }
            //right_check

            //
        }
        if(left_cnt == 0 || right_cnt == 0)
        {
            for(int j = 0; j < img.cols /2; j ++)
            {
                x = img.cols / 2 - j;
                if(img.at<unsigned char>(i, x))
                {
                    //points[point_index].x = j + range_rows;
                    points[point_index].x = - j - range_rows;
                    break;
                }
                x = img.cols / 2 + j;
                if(img.at<unsigned char>(i, x))
                {
                    //points[point_index].x = - j - range_rows;
                    points[point_index].x = j + range_rows;
                    break;
                }
            }
        }
        else if (right_cnt > left_cnt)
        {
            points[point_index].x = range_rows - left_cnt;
        }
        else if (right_cnt < left_cnt)
        {
            points[point_index].x = right_cnt - range_rows;

        }
        else if(right_cnt == right_cnt)
        {
            points[point_index].x = 0;
        }
        points[point_index].y = i;
        circle(trace, 2 * Point(img.cols / 2 + range_rows/*points[point_index].x*/, i), 4,Scalar(0, 255, 0));
        //cout << "i:" << "rig"
    }
    
    //imshow("trace", trace);
}

void planning_trace_optimize(point points[])
{
    point points_clone[200] = {};
    for(int i = 0; i < 200; i ++)
    {
        points_clone[i].x = points[i].x;
	points_clone[i].y = points[i].y;
    }
    for(int i = 1; i < 199; i ++)
    {
        points[i].x = (points_clone[i - 1].x + points_clone[i].x + points_clone[i + 1].x) / 3;
    }
}

void planning_point_generate(map plan_map, point points[])
{
    if(plan_map.read(0, 0))
    {
        points[0].x = 0;
        points[0].y = 0;
    }
    else
    {
        for (int i = -((MAP_WIDTH - 1) / 2); i < ((MAP_WIDTH - 1) / 2); i++)
        {
            if(plan_map.read(i, 0))
            {
                points[0].x = i;
                points[0].y = 0;
                break;
            }
            else if(plan_map.read(-i, 0))
            {
                points[0].x = -i;
                points[0].y = 0;
                break;
            }
            //TODO not found
        }
    }
    for(int i = 1; i < MAP_LENGTH / 10; i ++)
    {
        int y = i * 10;
        int last_x = points[i - 1].x;
        if(plan_map.read(points[i - 1].x, y))
        {
            points[i].x = last_x;
            points[i].y = y;
        }
        else
        {
            for (int j = 0; j < ((MAP_WIDTH - 1) / 2); j++)
            {
                if(plan_map.read(last_x + j, y))
                {
                    points[i].x = last_x + j;
                    points[i].y = y;
                    break;
                }
                else if(plan_map.read(last_x -j, y))
                {
                    points[i].x = last_x - j;
                    points[i].y = i;
                    break;
                }
            }
        }
    }
}

bool planning_zebra_check(Mat img)
{
    unsigned char color = 0;
    unsigned char last_color = 0;
    int counter = 0;
    int length = 0;
    int last_x = 0;
    for(int i = 0; i < 640; i++)
    {
        color = img.at<unsigned char>(300, i);
        if(last_color != color)
	{
	    length = i - last_x;
	    last_x = i;
	    if(length > 10 && length <60)counter++;
	    //std::cout << length << std::endl;
	}
        last_color = color;
    }
    //std::cout << "c:" << counter << std::endl;
    if(counter >= 12)return true;
    else return false;
}

int planning_circle_check(Mat img)
{
    static point left_points[360];
    static point right_points[360];
    bool left_break_flag = false;
    bool right_break_flag = false;
    int left_break_y = 0;
    int right_break_y = 0;
    Mat break_point = frame.clone();
    //left_points_scan;
    std::cout << "cicle check" << std::endl;
    for (int i = 0; i < 150; i++)
    {
        int y = 360 - i - 1;
        //std::cout << "y:" << y << std::endl;
        if(!img.at<unsigned char>(y, 320))
        {
            std::cout << "turn" << std::endl;
            return 0;
        }
        left_points[i].x = 1;
        right_points[i].x = 639;
        //left_points[360].y = i;
        //right_points[360].y = i;
        for(int j = 0; j < 320; j++)
        {
            //std::cout << 2 << std::endl;
            if(!img.at<unsigned char>(y, 320 - j))
            {
                left_points[i].x = - j;
            }

            //std::cout << 3 << std::endl;
            if(!img.at<unsigned char>(y,  320 + j))
            {
                right_points[i].x = j;
            }
        }
        for (int i = 0; i < 149; i++)
        {
            //left_break_check
            if(left_break_flag && right_break_flag)
            {
                std::cout << "cross" << std::endl;
                return 0;
            }
            if(left_points[i + 1].x - left_points[i].x > 50 && !left_break_flag)
            {
                left_break_flag = true;
                left_break_y = i;
                circle(break_point, 2 * Point(left_points[i].x, 360 - left_break_y), 4, Scalar(255, 0, 0));
            }

            if(right_points[i + 1].x - right_points[i].x < -50 && !right_break_flag)
            {
                right_break_flag = true;
                right_break_y = i;
                circle(break_point, 2 * Point(right_points[i].x , 360 - right_break_y), 10, Scalar(0, 0, 255));
            }
            
        }
    }
    imshow("break", break_point);
    if(right_break_flag && right_break_y < 100)
    {
        std::cout << "right circle" << std::endl;
        return 1;
    }
    else if(left_break_flag && left_break_y < 100)
    {
        std::cout << "left circle" << std::endl;
        return -1;
    }
    return 0;
    //right_points_scan;


    /*
    int counter = 0;
    for (int i = 0; i < num; i++)
    {
        if(img.at<unsigned char>(check_points[i].y, check_points[i].x))
        {
            counter ++;
        }
    }
    if(counter > 0.9 * num)
    {
        return true;
    }
    return false;*/
}

