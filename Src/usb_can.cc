#include "usb_can.hpp"
#include <iostream>
#include <fcntl.h>
#include <cstring>
#include <sys/select.h>
#include <unistd.h>
#include <queue>

using namespace std;

usb_can::usb_can()
{
    this -> fd = -1;
}

usb_can::usb_can(const char* path, int baud_rate)
{
    this -> fd = -1;
    this -> path = path;
    this -> baud_rate = baud_rate;
}

usb_can::~usb_can()
{
}

bool usb_can::usb_open()
{
    fd = open(this -> path.c_str(), O_RDWR);
    if(fd == -1)
    {
        std::cout << "open uart fail" << std::endl;
    }
    memset(&ios, 0, sizeof (ios));
    ios.c_cflag |= CLOCAL | CREAD;
    cfmakeraw(&ios);
    speed_t speed = baud_rate;
    cfsetispeed(&ios, speed);
    cfsetospeed(&ios, speed);
    ios.c_cflag &= ~CSIZE;
    ios.c_cflag |= CS8;
    ios.c_cflag &= ~PARENB;
    ios.c_cflag &= ~CSTOPB;
    ios.c_cc[VTIME] = 0;
    ios.c_cc[VMIN] = 1;
    tcflush(fd, TCIOFLUSH);
    tcsetattr(fd, TCSANOW, &ios);
    return USB_CAN_OK;
}

bool usb_can::usb_close()
{
    int ret;
 
    ret = close(fd);
    fd = -1;
 
    return USB_CAN_OK;
}

bool usb_can::data_write(unsigned char* data, int id, int length)
{
    int ret = -1;
    int value;
    unsigned char can_data[19];
    unsigned char buf[100];
    for(int i = 0; i < 9; i++)
    {
        can_data[i] = 0xFF;
    }
    can_data[9] = id >> 8;
    can_data[10] = id;
    memcpy(can_data + 11, data, 8);
    fd_set write_set;
    struct timeval tv;
 
    if (fd == -1)
    {
        return -1;
    }
    FD_ZERO(&write_set);
    FD_SET(fd, &write_set);
    tv.tv_sec = 1;
    tv.tv_usec = 0;
    ret = select(fd + 1, 0, &write_set, 0, &tv);
    switch (ret)
    {
        case 0:
            break;
        case -1:
            break;
        default:
            if (FD_ISSET(fd, &write_set))
            {
                ret = write(fd, can_data, length + 11);
            }
            break;
    }
    return USB_CAN_OK;
}

int usb_can::data_read(unsigned char* data, int* id)
{
    if (fd == -1)
    {
        return -1;
    }
    tcflush(fd, TCIFLUSH);// For most recent data
    unsigned char header_byte = 0;
    int header_counter = 0;
    while(header_counter < 10){
        read(fd, &header_byte, 1);
        if(header_byte == 0xFF)header_counter++;
        else
            header_counter == 0;
    }
    do
    {
        read(fd, &header_byte, 1);
    } while (header_byte > 0xF0);
    *id = (int)header_byte << 8;
    read(fd, &header_byte, 1);
    *id = (*id) | header_byte;
    
    read(fd, data, 8);
    return USB_CAN_OK;
}
