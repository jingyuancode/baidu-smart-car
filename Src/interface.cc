#include "interface.hpp"
#include "usb_can.hpp"
#include <iostream>
#include <cstring>
#include "unistd.h"

usb_can can("/dev/ttyACM0", 2000000);

float interface_angle_rate = 0;
float interface_speed = 0;

void interface_init()
{
    can.usb_open();
}

void interface_in()
{
    unsigned char data[8] = {};
    int id = 0;
    float angle_rate_raw = 0;
    float angle_rate = 0;
    float speed = 0;
    while (1)
    {
        can.data_read(data, &id);
        memcpy(&angle_rate_raw, data, 4);
        memcpy(&speed, data + 4, 4);
        memcpy(&interface_angle_rate, data, 4);
        memcpy(&interface_speed, data + 4, 4);
        angle_rate_raw = - angle_rate_raw;
        angle_rate = angle_rate * 0.5 + angle_rate_raw * 0.5;
        //speed = - speed;
        interface_angle_rate = angle_rate;
        interface_speed = speed;
        //std::cout << "interface_angle: " << interface_angle_rate << std::endl;
        //std::cout << "interface_speed: " << interface_speed << std::endl;
        usleep(1000);
    }
}

void interface_out(unsigned char* data)
{
    can.data_write(data, 0x111, 8);
    //std::cout << "interface_out:" << data << std::endl;
}
