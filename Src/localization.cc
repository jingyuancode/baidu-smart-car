#include "localization.hpp"
#include "interface.hpp"
#include "main.hpp"
#include <iostream>
#include <chrono>
#include "unistd.h"

float localization_odometer = 0;
float localization_yaw_angle = 0;

void localization()
{
    while (!ready);
    auto start_time = std::chrono::steady_clock::now();
    auto now_time = std::chrono::steady_clock::now();
    auto last_time = std::chrono::steady_clock::now();
    std::chrono::duration<double> duration = std::chrono::duration_cast<std::chrono::duration<double>>(now_time - last_time);
    double during_time = duration.count();
    while (1)
    {
        now_time = std::chrono::steady_clock::now();
        duration = std::chrono::duration_cast<std::chrono::duration<double>>(now_time - last_time);
        during_time = duration.count();
        localization_odometer += (during_time * interface_speed);
        localization_yaw_angle += (during_time * interface_angle_rate);
        last_time = now_time;
        //std::this_thread::sleep_for(std::chrono::microseconds(100));//useless by test
        usleep(10000);        
	//std::cout << "speed:" << interface_speed  << std::endl;
        //std::cout << "odom:" << localization_odometer << std::endl;//accuracy;
    }
    
}
