//range the distance to part area
#include "ranging.hpp"
#include "main.hpp"
#include <vector>

using namespace std;
using namespace cv;

float ranging(Mat frame, Mat cameraMatrix, float height)
{
    Mat img = frame.clone();
    Mat img_hsv;
    Mat img_hsv_list[3];
    Mat img_h_mask_high, img_h_mask_low, img_h_mask, img_s_mask, img_v_mask;
    Mat img_hs_mask;
    Mat img_yellow;
    Mat mask_xor;
    Mat target;
    int reference_pix_y = 0;
    float distance = 0;
    GaussianBlur(img, img, Size(3, 3), 15);
    cvtColor(img, img_hsv, COLOR_BGR2HSV);
    split(img_hsv, img_hsv_list);
    threshold(img_hsv_list[0], img_h_mask_low, 10, 255, THRESH_BINARY);
    threshold(img_hsv_list[0], img_h_mask_high, 30, 255, THRESH_BINARY_INV);
    threshold(img_hsv_list[1], img_s_mask, 50, 255, THRESH_BINARY);
    threshold(img_hsv_list[2], img_v_mask, 70, 255, THRESH_BINARY);

    bitwise_and(img_h_mask_high, img_h_mask_low, img_h_mask);
    bitwise_and(img_h_mask, img_s_mask, img_hs_mask);
    bitwise_and(img_hs_mask, img_v_mask, img_yellow);

    vector<vector<Point> > contours_points;
    findContours(img_yellow, contours_points, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);
    for (int i = 0; i < contours_points.size(); i++)
    {
        Rect box = boundingRect(contours_points[i]);
        if (box.y + box.height > reference_pix_y)
        {
            reference_pix_y = box.y + box.height;
        }
    }

    //Mat coordinate_img = (Mat_<double>(3,1) << 0, reference_pix_y, 1);
    //Mat coordinate_world = cameraMatrix.inv() * coordinate_img;// divided by z
    //distance = CAMERA_HEIGHT / coordinate_world.at<double>(1, 0) / 100;//100 for transform cm to m

    //TODO
    //cout << "distance" << distance << endl;
    //imshow("img_h_mask", img_h_mask); 
    //imshow("img_s_mask", img_s_mask); 
    //imshow("img_v_mask", img_v_mask);
    //imshow("yellow", img_yellow);
    return reference_pix_y;
}