#include "mapping.hpp"
#include "main.hpp"
#include "interface.hpp"
#include <chrono>
#include <iostream>

using namespace cv;

map mapping_local(CAMERA_HEIGHT);

void mapping()
{
    char frame_cnt_last = 0;
    while (!ready);
    frame_cnt_last = frame_cnt;
    Mat map_img, map_img_last;
    map_img = mapping_local.get_map();
    //rectangle(map_img, Point(100, 0));//TODO
    map_img_last = mapping_local.get_map();
    map frame_map(CAMERA_HEIGHT);

    auto now_time = std::chrono::steady_clock::now();
    auto last_time = std::chrono::steady_clock::now();
    std::chrono::duration<double> duration = std::chrono::duration_cast<std::chrono::duration<double>>(now_time - last_time);
    double during_time = duration.count();

    Mat img_runable;

    while (1)
    {
        while(frame_cnt_last == frame_cnt);
        frame_cnt_last = frame_cnt;
        //std::cout << frame_cnt << std::endl;
        
        now_time = std::chrono::steady_clock::now();
        duration = std::chrono::duration_cast<std::chrono::duration<double>>(now_time - last_time);
        during_time = duration.count();
        last_time = now_time;
        

        img_runable = mapping_img_preprocess(frame);
        
        frame_map.generate(img_runable, cameraMatrix / 2);
        
        float rotation_angle = during_time * interface_angle_rate * 180 / 3.14159265;
        //std::cout << rotation_angle << std::endl;
        
        Mat M = getRotationMatrix2D(Point(MAP_WIDTH / 2, 0), rotation_angle, 1);
        float translate = during_time * interface_speed * 100;//100 for m to cm
        M.at<double>(0, 2) = 0;
        M.at<double>(1, 2) = - translate;
        map_img = frame_map.get_map();
        //std::cout << translate << std::endl;
        //warpAffine(map_img_last, map_img_last, M, map_img_last.size(), INTER_NEAREST);
        //bitwise_or(map_img, map_img_last, map_img);
        imshow("frame_map", map_img);
        map_img_last = map_img.clone();
        mapping_local.set_map(map_img);
        //imshow("frame_map", map_img);
        
    }
    
}

Mat mapping_img_preprocess(Mat img)
{
    static Mat img_hsv;
    static Mat img_hsv_list[3];
    static Mat img_h_mask, img_s_mask, img_v_mask;
    static Mat img_h_mask_high, img_h_mask_low;
    static Mat img_sv_mask;
    static Mat img_white_mask, img_s_white_mask, img_v_white_mask;
    static Mat img_runable;

    resize(img, img, Size(0, 0), 0.5, 0.5);

    cvtColor(img, img_hsv, COLOR_BGR2HSV);
    split(img_hsv, img_hsv_list);
    threshold(img_hsv_list[2], img_v_mask, 100, 255, THRESH_BINARY);
    threshold(img_hsv_list[1], img_s_mask, 50, 255, THRESH_BINARY_INV);
    threshold(img_hsv_list[0], img_h_mask_high, 150, 255, THRESH_BINARY);
    threshold(img_hsv_list[0], img_h_mask_low, 85, 255, THRESH_BINARY_INV);
    threshold(img_hsv_list[1], img_s_white_mask, 30, 255, THRESH_BINARY_INV);
    threshold(img_hsv_list[2], img_v_white_mask, 190, 255, THRESH_BINARY);
    bitwise_and(img_s_white_mask, img_v_white_mask, img_white_mask);
    bitwise_or(img_h_mask_low, img_h_mask_high, img_h_mask);
    bitwise_and(img_s_mask, img_v_mask, img_sv_mask);
    bitwise_and(img_h_mask, img_sv_mask, img_runable);
    bitwise_or(img_white_mask, img_runable, img_runable);
    medianBlur(img_runable, img_runable, 3);
    //imshow("img_h_mask", img_h_mask); 
    //imshow("img_s_mask", img_s_mask); 
    //imshow("img_v_mask", img_v_mask);
    //imshow("img_run", img_runable);
    return img_runable.clone();
}